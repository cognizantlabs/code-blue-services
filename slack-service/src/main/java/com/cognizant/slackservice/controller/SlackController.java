package com.cognizant.slackservice.controller;

import com.cognizant.slackservice.model.SlackModel;
import com.cognizant.slackservice.service.SlackService;
import com.slack.api.Slack;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SlackController {

    @Autowired
    SlackService slackService;

    @GetMapping("/slack")
    @ResponseStatus(HttpStatus.OK)
    public ChatPostMessageResponse getSlackTeam(){
        return slackService.getSlackTeam();
    }

    @GetMapping("/hello")
    ResponseEntity<String> hello() {
        return new ResponseEntity<>("Hello World!", HttpStatus.OK);
    }

}
