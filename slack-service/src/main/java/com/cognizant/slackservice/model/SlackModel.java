package com.cognizant.slackservice.model;

public class SlackModel {

    private String toolType;
    private int boardId;

    public SlackModel() {
    }

    public SlackModel(String toolType, int boardId) {
        this.toolType = toolType;
        this.boardId = boardId;
    }

    public String getToolType() {
        return this.toolType;
    }

    public void setToolType(String toolType) {
        this.toolType = toolType;
    }

    public int getBoardId() {
        return this.boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    @Override
    public String toString() {
        return "Trello{" +
                "toolType = " + toolType +
                ", boardId = " + boardId +
                '}';
    }
}
