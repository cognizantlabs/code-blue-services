package com.cognizant.slackservice.service;

import com.cognizant.slackservice.model.SlackModel;
import com.slack.api.Slack;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;

public interface SlackService {
    ChatPostMessageResponse getSlackTeam();
}