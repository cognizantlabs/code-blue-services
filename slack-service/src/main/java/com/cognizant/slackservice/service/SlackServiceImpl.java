package com.cognizant.slackservice.service;

import com.cognizant.slackservice.model.SlackModel;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.slack.api.Slack;

@Component
public class SlackServiceImpl implements SlackService {

    private Slack slack;
    private ChatPostMessageResponse response;

    @Autowired
    public SlackServiceImpl() {
        slack = Slack.getInstance();
        MethodsClient methods = slack.methods("298105073111.1129438703348");
    }

    public ChatPostMessageResponse getSlackTeam() {
        try {
            response = Slack.getInstance().methods().chatPostMessage(
                    ChatPostMessageRequest.builder()
                            .channel("#random")
                            .text(":wave: Hi from a bot written in Java!")
                            .build()
            );
            return response;

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}