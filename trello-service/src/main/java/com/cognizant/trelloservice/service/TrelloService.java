package com.cognizant.trelloservice.service;

import com.cognizant.trelloservice.model.TrelloModel;
import com.julienvey.trello.domain.Board;

import java.util.List;

public interface TrelloService {
    Board getBoardById();
    TrelloModel addTrello(TrelloModel product);
    TrelloModel getTrelloById();
    List<TrelloModel> getAllTrellos();
}