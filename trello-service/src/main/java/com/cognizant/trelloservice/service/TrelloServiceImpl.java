package com.cognizant.trelloservice.service;


import com.julienvey.trello.domain.Board;
import com.julienvey.trello.impl.TrelloImpl;
import com.julienvey.trello.impl.http.ApacheHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import com.julienvey.trello.*;
import com.cognizant.trelloservice.model.TrelloModel;

@Component
public class TrelloServiceImpl implements TrelloService {

    private Trello trello;
    private TrelloHttpClient httpClient;

    @Autowired
    public TrelloServiceImpl() {
        trello = new TrelloImpl("799db5d749926b5cf56013e8d1229aff", "0fe90d3a6e9a1b5fb05246e9289f46b551f2635723ce0925b555040a14eba752", new ApacheHttpClient());
    }

    public Board getBoardById() {
        Board board = trello.getBoard("LQoXeQO1");
        System.out.println(board);
        return board;
    }

    public TrelloModel addTrello(TrelloModel trello){
        if (trello == null) {
            throw new IllegalArgumentException("You must enter a Trello");
        }

        return sampleTrello(trello);
    }

    public TrelloModel getTrelloById(){
        TrelloModel sample = new TrelloModel();
        sample.setBoardId(123);
        sample.setToolType("fooBar");
        return sampleTrello(sample);
    }

    public List<TrelloModel> getAllTrellos(){
        return trelloList();
    }

    public TrelloModel sampleTrello(TrelloModel trello) {
        trello.setBoardId(123);
        trello.setToolType("fooBar");
        return trello;
    }

    public List<TrelloModel> trelloList() {
        List<TrelloModel> trelloList = new ArrayList<>();
        TrelloModel trello = new TrelloModel();
        trello.setBoardId(123);
        trello.setToolType("fooBar");
        trelloList.add(trello);
        return trelloList;
    }
}