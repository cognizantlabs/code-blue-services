package com.cognizant.trelloservice.controller;

import com.cognizant.trelloservice.model.TrelloModel;
import com.cognizant.trelloservice.service.TrelloService;
import com.julienvey.trello.domain.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class TrelloController {

    @Autowired
    TrelloService trelloService;

    @GetMapping("/trello")
    @ResponseStatus(HttpStatus.OK)
    public Board getTrello(){
        return trelloService.getBoardById();
    }

    @GetMapping("/hello")
    ResponseEntity<String> hello() {
        return new ResponseEntity<>("Hello World!", HttpStatus.OK);
    }

}
